<?php
namespace EXOTEC\TestExample\Tests\Unit\Domain\Model;

/**
 * Test case.
 *
 * @author Alexander Weber <weber@exotec.de>
 */
class ModelTest extends \TYPO3\TestingFramework\Core\Unit\UnitTestCase
{
    /**
     * @var \EXOTEC\TestExample\Domain\Model\Model
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \EXOTEC\TestExample\Domain\Model\Model();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getTitleReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getTitle()
        );
    }

    /**
     * @test
     */
    public function setTitleForStringSetsTitle()
    {
        $this->subject->setTitle('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'title',
            $this->subject
        );
    }
}
