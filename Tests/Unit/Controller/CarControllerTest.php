<?php
namespace EXOTEC\TestExample\Tests\Unit\Controller;

use EXOTEC\TestExample\Domain\Model\Filter;
use EXOTEC\TestExample\Domain\Model\Make;
use EXOTEC\TestExample\Domain\Repository\MakeRepository;
use EXOTEC\TestExample\Domain\Repository\ModelRepository;
use Nimut\TestingFramework\TestCase\UnitTestCase;
use EXOTEC\TestExample\Controller\CarController;
use EXOTEC\TestExample\Domain\Model\Car;
use EXOTEC\TestExample\Domain\Repository\CarRepository;
use Prophecy\Prophecy\ObjectProphecy;
use Prophecy\Prophecy\ProphecySubjectInterface;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;
use TYPO3\CMS\Extbase\Persistence\QueryResultInterface;
use TYPO3\CMS\Fluid\View\TemplateView;

/**
 * Test case.
 *
 * @author Alexander Weber <weber@exotec.de>
 */
class CarControllerTest extends UnitTestCase
{
    /**
     * @var CarController
     */
    private $subject = null;

    /**
     * @var TemplateView|ObjectProphecy
     */
    private $viewProphecy = null;

    /**
     * @var CarRepository|ObjectProphecy
     */
    private $carRepositoryProphecy = null;

    protected function setUp(): void
    {
        $this->subject = $this->getMockBuilder(\EXOTEC\TestExample\Controller\CarController::class)
            ->setMethods(['redirect', 'forward', 'addFlashMessage'])
            ->disableOriginalConstructor()
            ->getMock();

        $this->viewProphecy = $this->prophesize(TemplateView::class);
        $view = $this->viewProphecy->reveal();
        $this->inject($this->subject, 'view', $view);

        $this->carRepositoryProphecy = $this->prophesize(CarRepository::class);
        /** @var CarRepository|ProphecySubjectInterface $carRepository */
        $carRepository = $this->carRepositoryProphecy->reveal();
        $this->subject->injectCarRepository($carRepository);

        $this->makeRepositoryProphecy = $this->prophesize(MakeRepository::class);
        /** @var MakeRepository|ProphecySubjectInterface $makeRepository */
        $makeRepository = $this->makeRepositoryProphecy->reveal();
        $this->subject->injectMakeRepository($makeRepository);

        $this->modelRepositoryProphecy = $this->prophesize(ModelRepository::class);
        /** @var ModelRepository|ProphecySubjectInterface $modelRepository */
        $modelRepository = $this->modelRepositoryProphecy->reveal();
        $this->subject->injectModelRepository($modelRepository);
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function is_there_an_action_controller(): void
    {
        self::assertInstanceOf(ActionController::class, $this->subject);
    }

    /**
     * @test
     */
    public function list_action_assigns_the_multiple_values_to_view(): void
    {
        $allCars = $this->prophesize(QueryResultInterface::class)->reveal();
        $this->carRepositoryProphecy->findAll()->willReturn($allCars);

        $allMakes = $this->prophesize(QueryResultInterface::class)->reveal();
        $this->makeRepositoryProphecy->findAll()->willReturn($allMakes);
        $makeSelectOptions = $this->subject->getSelectOptionItems($allMakes);
        $modelSelectOptions = array();

        $filter = new Filter();
        $values = array(
            'cars' => $allCars,
            'filter' => $filter,
            'makes' => $makeSelectOptions,
            'models' => $modelSelectOptions
        );

        $this->viewProphecy->assignMultiple($values)->shouldBeCalled();

        $this->subject->listAction();
    }

    /**
     * @test
     */
    public function show_action_assigns_passed_car_to_view(): void
    {
        $car = new Car();
        $this->viewProphecy->assign('car', $car)->shouldBeCalled();

        $this->subject->showAction($car);
    }

    /**
     * @test
     */
    public function edit_action_assigns_the_given_car_to_view()
    {
        $car = new \EXOTEC\TestExample\Domain\Model\Car();

        $makes = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->disableOriginalConstructor()
            ->getMock();
        $makeRepository = $this->getMockBuilder(\EXOTEC\TestExample\Domain\Repository\MakeRepository::class)
            ->setMethods(['findAll'])
            ->disableOriginalConstructor()
            ->getMock();
        $makeRepository->expects(self::once())->method('findAll')->will(self::returnValue($makes));
        $this->inject($this->subject, 'makeRepository', $makeRepository);

        $models = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->disableOriginalConstructor()
            ->getMock();
        $modelRepository = $this->getMockBuilder(\EXOTEC\TestExample\Domain\Repository\ModelRepository::class)
            ->setMethods(['findAll'])
            ->disableOriginalConstructor()
            ->getMock();
        $modelRepository->expects(self::once())->method('findAll')->will(self::returnValue($models));
        $this->inject($this->subject, 'modelRepository', $modelRepository);

        $makesOptions = $this->subject->getSelectOptionItems($makes);
        $modelsOptions = $this->subject->getSelectOptionItems($models);

        $values = array(
            'car' => $car,
            'makes' => $makesOptions,
            'models' => $modelsOptions
        );

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $this->inject($this->subject, 'view', $view);
        $view->expects(self::atLeastOnce())
            ->method('assignMultiple')->with($values);

        $this->subject->editAction($car);
    }

    /**
     * @test
     */
    public function update_action_updates_the_given_car_in_car_repository()
    {
        $car = new \EXOTEC\TestExample\Domain\Model\Car();

        $carRepository = $this->getMockBuilder(\EXOTEC\TestExample\Domain\Repository\CarRepository::class)
            ->setMethods(['update'])
            ->disableOriginalConstructor()
            ->getMock();

        $carRepository->expects(self::once())->method('update')->with($car);
        $this->inject($this->subject, 'carRepository', $carRepository);

        $this->subject->updateAction($car);
    }

    /**
     * @test
     */
    public function delete_action_deletes_the_given_car_from_car_repository()
    {
        $car = new \EXOTEC\TestExample\Domain\Model\Car();

        $carRepository = $this->getMockBuilder(\EXOTEC\TestExample\Domain\Repository\CarRepository::class)
            ->setMethods(['remove'])
            ->disableOriginalConstructor()
            ->getMock();

        $carRepository->expects(self::once())->method('remove')->with($car);
        $this->inject($this->subject, 'carRepository', $carRepository);

        $this->subject->deleteAction($car);
    }


    /* Additional (own) Controller Functions */

    /**
     * @test
     */
    public function get_select_option_items_returns_options_as_array()
    {
        /** @var ObjectStorage $objectStorage */
        $objectStorage = new ObjectStorage();

        $opel = new Make();
        $opel->setTitle('Opel Amazing title');
        $opel->setUid(1);
        $objectStorage->attach($opel);

        $audi = new Make();
        $audi->setTitle('Audi Amazing title');
        $audi->setUid(2);
        $objectStorage->attach($audi);

        $expected = array();
        foreach ($objectStorage as $entry) {
            $item = new \stdClass();
            $item->key = $entry->getUid();
            $item->value = $entry->getTitle();
            $expected[] = $item;
        }

        $actual = $this->subject->getSelectOptionItems($objectStorage);

        self::assertEquals(
            $expected,
            $actual
        );
        self::assertIsArray($actual);
    }

}
