<?php
declare(strict_types=1);

namespace EXOTEC\TestExample\Tests\Functional\Controller;

use EXOTEC\TestExample\Domain\Repository\CarRepository;
use Nimut\TestingFramework\TestCase\FunctionalTestCase;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Extbase\Persistence\Generic\Typo3QuerySettings;

/**
 * Test case.
 *
 * @author Alexander Weber <weber@exotec.de>
 */
class CarControllerTest extends FunctionalTestCase
{
    /**
     * @var string[]
     */
    protected $testExtensionsToLoad = ['typo3conf/ext/test_example'];

    /**
     * @var string[]
     */
    protected $coreExtensionsToLoad = ['fluid_styled_content'];

    protected function setUp(): void
    {
        parent::setUp();

        $this->importDataSet(__DIR__ . '/Fixtures/Database/pages.xml');
        $this->importDataSet(__DIR__ . '/../Domain/Repository/Fixtures/Car/Car.xml');

        $this->setUpFrontendRootPage(
            89,
            [
                'EXT:fluid_styled_content/Configuration/TypoScript/setup.typoscript',
                'EXT:test_example/Configuration/TypoScript/setup.typoscript',
                'EXT:test_example/Tests/Functional/Controller/Fixtures/Frontend/Basic.typoscript',
            ]
        );
    }

    /**
     * @test
     */
    public function list_action_renders_car_description(): void
    {
        $responseContent = $this->getFrontendResponse(89)->getContent();
        $carDescription = 'Bluemotion';
        self::assertContains($carDescription, $responseContent);
    }
}
