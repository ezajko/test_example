<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'EXOTEC.TestExample',
            'Pi1',
            [
                'Car' => 'list, show, new, create, edit, update, delete'
            ],
            // non-cacheable actions
            [
                'Car' => 'list, edit, create, update, delete'
            ]
        );

    // wizards
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    pi1 {
                        iconIdentifier = test_example-plugin-pi1
                        title = LLL:EXT:test_example/Resources/Private/Language/locallang_db.xlf:tx_test_example_pi1.name
                        description = LLL:EXT:test_example/Resources/Private/Language/locallang_db.xlf:tx_test_example_pi1.description
                        tt_content_defValues {
                            CType = list
                            list_type = testexample_pi1
                        }
                    }
                }
                show = *
            }
       }'
    );
		$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
		
			$iconRegistry->registerIcon(
				'test_example-plugin-pi1',
				\TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
				['source' => 'EXT:test_example/Resources/Public/Icons/user_plugin_pi1.svg']
			);
		
    }
);
