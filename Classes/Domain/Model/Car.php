<?php
namespace EXOTEC\TestExample\Domain\Model;


/***
 *
 * This file is part of the "Test example" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 Alexander Weber <weber@exotec.de>, exotec
 *
 ***/
/**
 * Car
 */
class Car extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * description
     * 
     * @var string
     */
    protected $description = '';

    /**
     * make
     * 
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\EXOTEC\TestExample\Domain\Model\Make>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $make = null;

    /**
     * model
     * 
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\EXOTEC\TestExample\Domain\Model\Model>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $model = null;

    /**
     * Returns the description
     * 
     * @return string $description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Sets the description
     * 
     * @param string $description
     * @return void
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * __construct
     */
    public function __construct()
    {

        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }

    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     *
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->make = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->model = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

    /**
     * Adds a Make
     * 
     * @param \EXOTEC\TestExample\Domain\Model\Make $make
     * @return void
     */
    public function addMake(\EXOTEC\TestExample\Domain\Model\Make $make)
    {
        $this->make->attach($make);
    }

    /**
     * Removes a Make
     * 
     * @param \EXOTEC\TestExample\Domain\Model\Make $makeToRemove The Make to be removed
     * @return void
     */
    public function removeMake(\EXOTEC\TestExample\Domain\Model\Make $makeToRemove)
    {
        $this->make->detach($makeToRemove);
    }

    /**
     * Returns the make
     * 
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\EXOTEC\TestExample\Domain\Model\Make> $make
     */
    public function getMake()
    {
        return $this->make;
    }

    /**
     * Sets the make
     * 
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\EXOTEC\TestExample\Domain\Model\Make $make
     * @return void
     */
    public function setMake(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $make)
    {
        $this->make = $make;
    }

    /**
     * Adds a Model
     * 
     * @param \EXOTEC\TestExample\Domain\Model\Model $model
     * @return void
     */
    public function addModel(\EXOTEC\TestExample\Domain\Model\Model $model)
    {
        $this->model->attach($model);
    }

    /**
     * Removes a Model
     * 
     * @param \EXOTEC\TestExample\Domain\Model\Model $modelToRemove The Model to be removed
     * @return void
     */
    public function removeModel(\EXOTEC\TestExample\Domain\Model\Model $modelToRemove)
    {
        $this->model->detach($modelToRemove);
    }

    /**
     * Returns the model
     * 
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\EXOTEC\TestExample\Domain\Model\Model> $model
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Sets the model
     * 
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\EXOTEC\TestExample\Domain\Model\Model> $model
     * @return void
     */
    public function setModel(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $model)
    {
        $this->model = $model;
    }
}
